extends Area2D

var speed = 50
export (PackedScene) var Hpart

func _ready():
	$Sprite.rotation -= rotation

func _physics_process(delta):
	position += transform.x * speed * delta

func _on_Bullet_body_entered(body):
	if body.is_in_group("Player"):
		body.health -= 1
		for i in get_tree().get_nodes_in_group("cameras"):
			i.add_trauma(0.5)
	queue_free()
	var a = Hpart.instance()
	a.position = global_position
	a.rotation = global_rotation
	get_tree().get_root().add_child(a)
	queue_free()
