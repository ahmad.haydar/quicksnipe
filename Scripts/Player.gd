extends KinematicBody2D

export (int) var speed = 100
export (int) var jump_speed = -200
export (int) var gravity = 500
export (float, 0, 1.0) var friction = 0.1
export (float, 0, 1.0) var acceleration = 0.25
var jumpavailability = false
var canshoot = true
var jumpval = 0
var pointbuy = 20
export (float) var max_health = 20
export (float) var max_stamina = 9
var stamina
var health
var velocity = Vector2.ZERO
export (PackedScene) var Bullet
export (PackedScene) var MuzzleFlash
export (PackedScene) var WalkParticle
export (PackedScene) var Turret
export (NodePath) var flashing
var zoomed_in = false
var timescale = 0.2
func _ready():
	health = max_health
	stamina = max_stamina
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	
func get_input():
	var dir = 0
	if Input.is_action_pressed("walk_right"):
		dir += 1
	if Input.is_action_pressed("walk_left"):
		dir -= 1
	if dir != 0:
		velocity.x = lerp(velocity.x, dir * speed, acceleration)
		$AnimationPlayer.play("move")
	else:
		velocity.x = lerp(velocity.x, 0, friction)
		$AnimationPlayer.play("idle")
	if Input.is_action_just_pressed("shoot"):
		for i in get_tree().get_nodes_in_group("cameras"):
			i.trauma = 0
			i.add_trauma(0.4)
			break
		shoot()
	aiming()
	if Input.is_action_just_pressed("deploy") and pointbuy >= 8 and is_on_floor():
		pointbuy -= 8
		var b = Turret.instance()
		SceneManager._current_scene.add_child(b)
		b.global_position = global_position
	if Input.is_action_just_pressed("emp") and pointbuy >= 20:
		pointbuy -= 20
		get_node(flashing).play("flash")
		get_tree().call_group("Bullets", "queue_free")
		
func aiming():
	$Sprite.flip_h = global_position.x - get_global_mouse_position().x > 0
	$GunSprite.flip_v = global_position.x - get_global_mouse_position().x > 0
	$GunSprite.look_at(get_global_mouse_position())
	if $GunSprite.flip_v:
		$GunSprite.offset.y = -2.5
	else:
		$GunSprite.offset.y = 2.5

func _physics_process(delta):
	if health <= 0:
		Bgm.ls = owner.surviving_score
		if Bgm.ls > Bgm.hs:
			Bgm.hs = Bgm.ls
		SceneManager.change_scene("res://Scenes/Menu.tscn")
	get_input()
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	if is_on_floor():
		if jumpval!= 0:
			jumpval = 0
			$ScaleAnim.play("fall")
		jumpavailability = true
	elif jumpavailability and $Timer.is_stopped():
		$Timer.start()
	if zoomed_in:
		Bgm.pitch_scale = lerp(Bgm.pitch_scale, 0.2, 0.2)
		if stamina > 0:
			stamina = stamina - 0.05
		elif stamina < 0:
			stamina = 0
		if stamina == 0:
			zoomed_in = false
			Engine.time_scale = 1
		$Camera2D.zoom.x = lerp($Camera2D.zoom.x, 0.8, 0.05)
		$Camera2D.zoom.y = lerp($Camera2D.zoom.y, 0.8, 0.05)
	elif !zoomed_in:
		Bgm.pitch_scale = lerp(Bgm.pitch_scale, 1, 0.2)
		if stamina < max_stamina:
			stamina = stamina + 0.05
		elif stamina > max_stamina:
			stamina = max_stamina
		$Camera2D.zoom.x = lerp($Camera2D.zoom.x, 1, 0.05)
		$Camera2D.zoom.y = lerp($Camera2D.zoom.y, 1, 0.05)
		
	bullet_time_set()
	if Input.is_action_just_pressed("jump"):
		if jumpavailability:
			velocity.y = jump_speed
			jumpval = 1
			$ScaleAnim.play("jump")
		elif jumpval < 2:
			velocity.y = jump_speed
			jumpval = 2
			$ScaleAnim.play("jump")

func bullet_time_set():
	if Input.is_action_just_pressed("aim"):
		Engine.time_scale = timescale
		zoomed_in = true
	if Input.is_action_just_released("aim"):
		zoomed_in = false
		Engine.time_scale = 1

func shoot():
	if !canshoot:
		return
	$AudioStreamPlayer2D2.play()
	var b = Bullet.instance()
	owner.add_child(b)
	var a = MuzzleFlash.instance()
	$GunSprite/Muzzle.add_child(a)
	b.transform = $GunSprite/Muzzle.global_transform
	b.apply_impulse(Vector2(), Vector2(500,0).rotated($GunSprite.rotation))
	canshoot = false
	$ShootTimer.start()

func _on_Timer_timeout():
	jumpavailability = false

func walk():
	if !is_on_floor():
		return
	var b = WalkParticle.instance()
	owner.add_child(b)
	b.global_position = $Feet.global_position
	
func _on_ShootTimer_timeout():
	canshoot = true
