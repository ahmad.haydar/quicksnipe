extends Area2D

var speed = 100
export (PackedScene) var Hpart

func _physics_process(delta):
	position += transform.x * speed * delta

func _on_Bullet_body_entered(body):
	if body.is_in_group("mobs"):
		body.health -=1
		body.get_node("Flash").play("hurt")
		for i in get_tree().get_nodes_in_group("cameras"):
			i.add_trauma(0.2)
	if body.name == "Player":
		body.health -=1
		for i in get_tree().get_nodes_in_group("cameras"):
			i.add_trauma(0.2)
	var a = Hpart.instance()
	a.position = $Position2D.global_position
	a.rotation = $Position2D.global_rotation
	get_tree().get_root().add_child(a)
	queue_free()


func _on_Timer_timeout():
	queue_free()
