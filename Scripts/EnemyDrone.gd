extends KinematicBody2D

var run_speed = 50
var health = 5
var velocity = Vector2.ZERO
var player = null
var target = null
export (PackedScene) var explosion
export (PackedScene) var bullet

func _ready():
	for i in get_tree().get_nodes_in_group("Player"):
		player = i
		target = i
		break
	$AnimationPlayer.play("wep")

func _physics_process(_delta):
	if health <= 0:
		var b = explosion.instance()
		get_tree().get_root().add_child(b)
		b.transform = global_transform
		for i in get_tree().get_nodes_in_group("cameras"):
			i.add_trauma(4)
		target.pointbuy += 1
		queue_free()
	if is_instance_valid(player):
		velocity = lerp(velocity, position.direction_to(player.position) * run_speed, 0.1)
	else:
		velocity = lerp(velocity, Vector2.ZERO, 0.05)
	velocity = move_and_slide(velocity)

func _on_DetectRadius_body_entered(body):
	if body.name == "Player":
		player = null

func white():
	get_node("Sprite").modulate = Color(10,10,10,10)

func nowhite():
	get_node("Sprite").modulate = Color(1,1,1,1)

func _on_DetectRadius_body_exited(body):
	if body.name == "Player":
		player = body


func _on_Timer_timeout():
	for i in range(8):
		var b = bullet.instance()
		SceneManager._current_scene.add_child(b)
		b.transform = global_transform
		b.rotation_degrees = 45*i
		$AudioStreamPlayer2D.play()
