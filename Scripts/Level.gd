extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var enem_1 = preload("res://Scenes/EnemDrone.tscn")
var enem_2 = preload("res://Scenes/EnemShooter.tscn")
var enem_3 = preload("res://Scenes/EnemWatcher.tscn")
var tutorial1 = preload("res://Scenes/Tutorial1.tscn")
var enems = []
var enemy_count = 0
var surviving_score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	enems = [enem_1, enem_2, enem_3]
	$CanvasLayer/Tuttur.modulate.a = 0.1
	var t = tutorial1.instance()
	$Player.add_child(t)
	t.global_position = $Player.global_position
	t.global_position.y -= 20
	randomize()

func _physics_process(_delta):
	if $Player.pointbuy >= 8:
		$CanvasLayer/Tuttur.modulate.a = lerp($CanvasLayer/Tuttur.modulate.a, 1, 0.1)
	else:
		$CanvasLayer/Tuttur.modulate.a = lerp($CanvasLayer/Tuttur.modulate.a, 0.1, 0.1)
	if $Player.pointbuy >= 20:
		$CanvasLayer/Tutemp.modulate.a = lerp($CanvasLayer/Tutemp.modulate.a, 1, 0.1)
	else:
		$CanvasLayer/Tutemp.modulate.a = lerp($CanvasLayer/Tutemp.modulate.a, 0.1, 0.1)
	if enemy_count <= 15:
		$Enemy_spawn_timer.wait_time = 4
	elif enemy_count <= 45:
		$Enemy_spawn_timer.wait_time = 2
	elif enemy_count <= 80:
		$Enemy_spawn_timer.wait_time = 1
	else:
		$Enemy_spawn_timer.wait_time = 0.8

func _on_Enemy_spawn_timer_timeout():
	var enemy_global_position = Vector2(rand_range(-160, 670), rand_range(-90, -50))
	var enemz = enems[int(rand_range(0,3))]
	var b = enemz.instance()
	add_child(b)
	b.global_position = enemy_global_position


func _on_Area2D_body_entered(body):
	if body.name == "Player":
		body.health = 0


func _on_Seconds_Timer_timeout():
	surviving_score += 1
