extends KinematicBody2D

var run_speed = 30
var health = 5
var velocity = Vector2.ZERO
var target = null
var player = null
export (PackedScene) var explosion
export (PackedScene) var bullet
export (int) var rotationSpeed = 3
func _ready():
	for i in get_tree().get_nodes_in_group("Player"):
		player = i
		target = i
		break

func aiming():
	$Sprite.flip_h = global_position.x - target.global_position.x < 0
	
func _physics_process(delta):
	aiming()
	if health <= 0:
		var b = explosion.instance()
		get_tree().get_root().add_child(b)
		b.transform = global_transform
		for i in get_tree().get_nodes_in_group("cameras"):
			i.add_trauma(4)
		target.pointbuy += 2
		queue_free()
	if is_instance_valid(player):
		velocity = lerp(velocity, position.direction_to(player.position) * run_speed, 0.1)
	else:
		velocity = lerp(velocity, Vector2.ZERO, 0.05)
	velocity = move_and_slide(velocity)

func _on_DetectRadius_body_entered(body):
	if body.name == "Player":
		player = null

func white():
	get_node("Sprite").modulate = Color(10,10,10,10)

func nowhite():
	get_node("Sprite").modulate = Color(1,1,1,1)

func _on_DetectRadius_body_exited(body):
	if body.name == "Player":
		player = body

func _on_Enem_Timer_timeout():
	if !target:
		return
	for i in range(3):
		var b = bullet.instance()
		SceneManager._current_scene.add_child(b)
		b.speed = 50
		b.transform = global_transform
		b.look_at(target.global_position)
		b.rotation_degrees += 5 - i*5
		$AudioStreamPlayer2D.play()
