extends KinematicBody2D

var player = null
export (PackedScene) var bullet
export (int) var rotationSpeed = 5

func _physics_process(_delta):
	if !is_instance_valid(player):
		for i in $DetectRadius.get_overlapping_bodies():
			if i.is_in_group("mobs"):
				player = i
				break
	if is_instance_valid(player):
		$GunSprite.look_at(player.global_position)
		$GunSprite/ColorRect.margin_right = $GunSprite/Muzzle.global_position.distance_to(player.global_position)
	else:
		$GunSprite/ColorRect.margin_right = 0

func _on_DetectRadius_body_entered(body):
	if body.is_in_group("mobs"):
		player = body

func _on_DetectRadius_body_exited(body):
	if body.is_in_group("mobs"):
		player = null
		
func _on_Timer_timeout():
	if !player:
		return
	for _i in range(1):
		var b = bullet.instance()
		SceneManager._current_scene.add_child(b)
		b.speed = 100
		b.transform = $GunSprite/Muzzle.global_transform


func _on_Timer2_timeout():
	queue_free()
