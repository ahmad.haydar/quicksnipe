extends KinematicBody2D

var run_speed = 50
var health = 5
var velocity = Vector2.ZERO
var target = null
var player = null
export (PackedScene) var explosion
export (PackedScene) var bullet
export (int) var rotationSpeed = 3
var dir
func _ready():
	for i in get_tree().get_nodes_in_group("Player"):
		player = i
		target = i
		break

func aiming():
	$Sprite.flip_h = global_position.x - target.global_position.x > 0
	if $Sprite.flip_h:
		dir = -1
	else:
		dir = 1
	$RayCast2D2.cast_to.x = 10*dir
	$GunSprite.flip_v = rotation_degrees > 90
	if $GunSprite.flip_v:
		$GunSprite.offset.y = -2.5
	else:
		$GunSprite.offset.y = 2.5

func rotateToTarget(targ, delta):
	var direction = (targ.global_position - global_position)
	var angleTo = $GunSprite.transform.x.angle_to(direction)
	$GunSprite.rotate(sign(angleTo) * min(delta * rotationSpeed, abs(angleTo)))

func _physics_process(delta):
	aiming()
	if is_instance_valid(target):
		rotateToTarget(target, delta)
	if health <= 0:
		var b = explosion.instance()
		get_tree().get_root().add_child(b)
		b.transform = global_transform
		for i in get_tree().get_nodes_in_group("cameras"):
			i.add_trauma(4)
		target.pointbuy += 2
		queue_free()
	if is_instance_valid(player):
		if $RayCast2D.is_colliding() == false:
			velocity = lerp(velocity, position.direction_to(player.position) * run_speed, 0.1)
		elif $RayCast2D.get_collider().name == "StaticBody2D":
			velocity.x = lerp(velocity.x, dir * run_speed, 0.1)
			velocity.y += 500 * delta
			if $RayCast2D2.is_colliding() and $RayCast2D.get_collider().name == "StaticBody2D":
				velocity.y = -200
	else:
		velocity = lerp(velocity, Vector2.ZERO, 0.05)
		if $RayCast2D.is_colliding():
			if $RayCast2D.get_collider().name == "StaticBody2D":
				velocity.x = lerp(velocity.x, 0, 0.05)
				velocity.y += 500 * delta
	if $RayCast2D.is_colliding():
		$CPUParticles2D.emitting = false
	elif $RayCast2D.is_colliding() == false:
		$CPUParticles2D.emitting = true
	velocity = move_and_slide(velocity)

func _on_DetectRadius_body_entered(body):
	if body.name == "Player":
		player = null

func white():
	get_node("Sprite").modulate = Color(10,10,10,10)

func nowhite():
	get_node("Sprite").modulate = Color(1,1,1,1)

func _on_DetectRadius_body_exited(body):
	if body.name == "Player":
		player = body

func _on_Enem_Timer_timeout():
	if !target:
		return
	for _i in range(3):
		var b = bullet.instance()
		SceneManager._current_scene.add_child(b)
		b.speed = 150
		b.transform = $GunSprite/Muzzle.global_transform
		$AudioStreamPlayer2D.play()
		$Timer.start(0.2); yield($Timer, "timeout")
